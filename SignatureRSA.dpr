{*******************************************************************
@file    SignatureRSA.dpr
@author  Sadakov Oleg <olegsadakov.work@gmail.com>
@license LGPLv3, 29 June 2007

@section LICENSE

Application for the creation of the key pair,
signature creation and verification.

Copyright (C) 2013 Sadakov Oleg <olegsadakov.work@gmail.com>

GNU LESSER GENERAL PUBLIC LICENSE

Version 3, 29 June 2007

Copyright � 2007 Free Software Foundation, Inc. <http://fsf.org/>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************}

program SignatureRSA;

uses
  Forms,
  SysUtils,
  Dialogs,
  Classes,
  Windows,
  Form in 'Form.pas' {Form1},
  CryptoUnit in 'CryptoUnit.pas',
  Wcrypt2 in 'Wcrypt2.pas',
  Language in 'Language.pas';

{$R *.res}

type
  ETypeParam = (tEmpty, tAction, tKey, tTarget, tSignature);

function ParseParameters: Boolean;
var
  I: Integer;
  Param: String;
  TypeParam: ETypeParam;
  Action, Key, Target, Signature: String;
  IncorrectList: TStringList;
begin
  Result := False;
  Action := '';
  Key := '';
  Target := '';
  Signature := '';
  TypeParam := tEmpty;
  IncorrectList := TStringList.Create;
  try
    for I := 0 to ParamCount do
    begin
      Param := AnsiLowerCase(ParamStr(I));
      if (Param = 'action') or (Param = '-a') then
        TypeParam := tAction
      else if (Param = 'key') or (Param = '-k') then
        TypeParam := tKey
      else if (Param = 'target') or (Param = '-t') then
        TypeParam := tTarget
      else if (Param = 'signature') or (Param = '-s') then
        TypeParam := tSignature
      else
      begin
        case TypeParam of
        tAction:    Action    := Param;
        tKey:       Key       := ParamStr(I);
        tTarget:    Target    := ParamStr(I);
        tSignature: Signature := ParamStr(I);
        else
          if I <> 0 then
            IncorrectList.Add(ParamStr(I));
        end;
        TypeParam := tEmpty;
      end;
    end;
    if (ParamCount<=1) and (IncorrectList.Count = 0) then
      exit;

    if (Action    <> '') and
       (Key       <> '') and
       (Target    <> '') and
       (Signature <> '') then
    begin
      if Action = 'sign' then
      begin
        try
          Sign(Key, Target, Signature);
        except
          on e: Exception do
            ShowMessage(e.Message);
        end;
        Result := True;
        exit;
      end;

      if Action = 'verify' then
      begin
        try
          Verify(Key, Target, Signature);
        except
          on e: Exception do
            ShowMessage(e.Message);
        end;
        Result := True;
        exit;
      end;

      IncorrectList.Add(Action);
    end;

    if IncorrectList.Count = 0 then
      MessageBox(0,
                 PChar(T(tUseApplication)),
                 PChar(T(tTitleApplication)),
                 MB_ICONINFORMATION or MB_OK)
    else
    begin
      MessageBox(0,
                 PChar(T(tUseApplication)+#13#10#13#10+
                       T(tIncorrectParameters)+#13#10+
                       IncorrectList.Text),
                 PChar(T(tTitleApplication)),
                 MB_ICONWARNING or MB_OK);
      Result := True;
    end;
  finally
    FreeAndNil(IncorrectList);
  end;
end;

begin
  Application.Initialize;

  if ParseParameters then
    exit;

  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
