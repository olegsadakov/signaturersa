{*******************************************************************
@file    CryptoUnit.pas
@author  Sadakov Oleg <olegsadakov.work@gmail.com>
@license LGPLv3, 29 June 2007

@section LICENSE

Application for the creation of the key pair,
signature creation and verification.

Copyright (C) 2013 Sadakov Oleg <olegsadakov.work@gmail.com>

GNU LESSER GENERAL PUBLIC LICENSE

Version 3, 29 June 2007

Copyright � 2007 Free Software Foundation, Inc. <http://fsf.org/>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************}

unit CryptoUnit;

interface

procedure CreatePair(PublicKey, PublicKeyInc, PrivateKey: String);
procedure Sign(PrivateKey: String; Target: String; Signature: String);
function  Verify(PublicKey: String; Target: String; Signature: String): Boolean;

implementation

uses
  Dialogs, Windows, SysUtils, Classes,

  Wcrypt2, Language;

const
  // �������� ���������� ���������� ��� �������� ������
  // (����� ���, ���������� ���������� ��� ���� �������)
  CONTAINER = 'Container.ForKeyPair.Signature';
  // ������ ����� ��� RSA
  RSAKEYSIZE = 2048;
  // �������� ���������� ����
  ALGID_HASH = CALG_SHA;
  //ALGID_HASH = CALG_MD5;

  // �������/������ ���������� �����:
  // - �������� ��� �������� ����
  PRIVATEKEY_ALGID_HASH = CALG_SHA;
  // - �������� ��� �������� ����� �� ����
  PRIVATEKEY_ALGID_KEY = CALG_RC4;

////////////////////////////////////////////////////////////////////////////////
  
procedure ExportKey(KeyFile: String;
                    hKey: HCRYPTKEY; dwBlobType: Cardinal;
                    hExpKey: HCRYPTKEY);
var
  Size: Cardinal;
  key: TMemoryStream;
begin
  key := nil;
  try
    key := TMemoryStream.Create;
    if not CryptExportKey(hKey, hExpKey, dwBlobType, 0, nil, @Size) then
      raise Exception.Create(SysErrorMessage(GetLastError));
    key.SetSize(Size);
    if not CryptExportKey(hKey, hExpKey, dwBlobType, 0, key.Memory, @Size) then
      raise Exception.Create(SysErrorMessage(GetLastError));
    key.SaveToFile(KeyFile);
  finally
    FreeAndNil(key);
  end;
end;

function IntToStrLen(value: Integer; Len: Integer): String;
begin
  Result := IntToStr(value);
  while Length(Result)<Len do
    Result := '0'+Result;
end;

procedure ExportKeyInc(KeyFile: String;
                       hKey: HCRYPTKEY; dwBlobType: Cardinal;
                       hExpKey: HCRYPTKEY);
var
  I, Size: Cardinal;
  key: TMemoryStream;
  inc: TStringStream;
  buf: PByte;
begin
  key := nil;
  inc := nil;
  try
    key := TMemoryStream.Create;
    if not CryptExportKey(hKey, hExpKey, dwBlobType, 0, nil, @Size) then
      raise Exception.Create(SysErrorMessage(GetLastError));
    key.SetSize(Size);
    if not CryptExportKey(hKey, hExpKey, dwBlobType, 0, key.Memory, @Size) then
      raise Exception.Create(SysErrorMessage(GetLastError));

    buf := key.Memory;
    inc := TStringStream.Create;
    try
      inc.WriteString('const ALGID = '+IntToStr(ALGID_HASH)+';'+#13#10);
      inc.WriteString('const PUBLIC_KEY = '+#13#10);
      for i := 0 to Size do
      begin
        if i = 0 then
          inc.WriteString('  ')
        else if i and 15 = 0 then
          inc.WriteString('+'+#13#10+'  ');
        inc.WriteString('#'+IntToStrLen(buf[i],3));
      end;
      inc.WriteString(';'+#13#10);
      inc.SaveToFile(KeyFile);
    finally
      FreeAndNil(inc);
    end;
  finally
    FreeAndNil(key);
  end;
end;

procedure ImportKey(KeyFile: String;
                    hProv: HCRYPTPROV;
                    var hKey: HCRYPTKEY;
                    hExpKey: HCRYPTKEY);
var
  key: TMemoryStream;
begin
  key := nil;
  try
    key := TMemoryStream.Create;
    key.LoadFromFile(KeyFile);
    key.Position := 0;
    if not CryptImportKey(hProv,
                          key.Memory, key.Size,
                          hExpKey,
                          CRYPT_EXPORTABLE, @hKey) then
      raise Exception.Create(SysErrorMessage(GetLastError));
  finally
    FreeAndNil(key);
  end;
end;

procedure CreateHash(Target: String;
                     hProv: HCRYPTPROV;
                     var hHash: HCRYPTHASH);
var
  InputStream: TMemoryStream;
begin
  hHash := 0;
  if not CryptCreateHash(hProv, ALGID_HASH, 0, 0, @hHash) then
    raise Exception.Create(SysErrorMessage(GetLastError));

  InputStream := TMemoryStream.Create;
  try
    InputStream.LoadFromFile(Target);

    if not CryptHashData(hHash, PByte(InputStream.Memory), InputStream.Size, 0) then
      raise Exception.Create(SysErrorMessage(GetLastError));
  finally
    FreeAndNil(InputStream);
  end;
end;

procedure CreateHashByString(Source: String;
                             hProv: HCRYPTPROV;
                             var hHash: HCRYPTHASH);
var
  InputStream: TStringStream;
begin
  hHash := 0;
  if not CryptCreateHash(hProv, PRIVATEKEY_ALGID_HASH, 0, 0, @hHash) then
    raise Exception.Create(SysErrorMessage(GetLastError));

  InputStream := TStringStream.Create;
  try
    while InputStream.Size<10*1024 do
      InputStream.WriteString(Source);

    if not CryptHashData(hHash, InputStream.Memory, InputStream.Size, 0) then
      raise Exception.Create(SysErrorMessage(GetLastError));
  finally
    FreeAndNil(InputStream);
  end;
end;

////////////////////////////////////////////////////////////////////////////////

procedure CreatePair(PublicKey, PublicKeyInc, PrivateKey: String);
var
  hProv:    HCRYPTPROV;
  hKey:     HCRYPTKEY;
  hKeyExp:  HCRYPTKEY;
  hHashExp: HCRYPTHASH;
  password: String;
begin
  if FileExists(PublicKey) and not DeleteFile(PublicKey) then
  begin
    ShowMessage(T(eCantRemovePublicKey));
    exit;
  end;
  if FileExists(PublicKeyInc) and not DeleteFile(PublicKeyInc) then
  begin
    ShowMessage(T(eCantRemovePublicKeyInc));
    exit;
  end;
  if FileExists(PrivateKey) and not DeleteFile(PrivateKey) then
  begin
    ShowMessage(T(eCantRemovePrivateKey));
    exit;
  end;

  try
    // ������� ���������, ���� �� �����������
    CryptAcquireContext(@hProv, CONTAINER, nil, PROV_RSA_FULL,
                        CRYPT_DELETEKEYSET);

    // ������� ��������� ��� �������� ����
    if not CryptAcquireContext(@hProv, CONTAINER, nil, PROV_RSA_FULL, 0) then
      if not CryptAcquireContext(@hProv, CONTAINER, nil, PROV_RSA_FULL,
                                 CRYPT_NEWKEYSET) then
        raise Exception.Create(SysErrorMessage(GetLastError));

    Password := InputBox(T(mCreateKeyPair),
                         T(mInputPasswordForPrivateKey),
                         '');
    if Password = '' then
      raise Exception.Create(T(eEmptyPasswordForPrivateKey));

    // ������� ��� �� ������ ��� �������� ���������� �����
    CreateHashByString(password, hProv, hHashExp);
    try
      // ������� ��� �� ���� ��� �������� ���������� �����
      if not CryptDeriveKey(hProv, PRIVATEKEY_ALGID_KEY, hHashExp, 0, @hKeyExp) then
        raise Exception.Create(SysErrorMessage(GetLastError));
    finally
      // ������� ���
      if not CryptDestroyHash(hHashExp) then
        raise Exception.Create(SysErrorMessage(GetLastError));
    end;

    try
      // ������� �������� ���� ��� ��������
      if not CryptGenKey(hProv, AT_SIGNATURE,
                         (RSAKEYSIZE shl 16) or CRYPT_EXPORTABLE,
                         @hKey) then
        raise Exception.Create(SysErrorMessage(GetLastError));

      try
        // �������������� ��������� ����
        ExportKey(PublicKey, hKey, PUBLICKEYBLOB, 0);
        ExportKeyInc(PublicKeyInc, hKey, PUBLICKEYBLOB, 0);

        // �������������� ��������� ����
        ExportKey(PrivateKey, hKey, PRIVATEKEYBLOB, hKeyExp);
      finally
        // ������� �������� ����
        if not CryptDestroyKey(hKey) then
          raise Exception.Create(SysErrorMessage(GetLastError));
      end;
    finally
        // ������� ���� ��� �������� ���������� �����
        if not CryptDestroyKey(hKeyExp) then
          raise Exception.Create(SysErrorMessage(GetLastError));
    end;
  finally
    // ���������� ���������
    if not CryptReleaseContext(hProv, 0) then
      raise Exception.Create(SysErrorMessage(GetLastError));
    // ������� ����� �� ����������
    if not CryptAcquireContext(@hProv, CONTAINER, nil, PROV_RSA_FULL,
                               CRYPT_DELETEKEYSET) then
      raise Exception.Create(SysErrorMessage(GetLastError));
  end;
end;

procedure Sign(PrivateKey: String; Target: String; Signature: String);
var
  hProv: HCRYPTPROV;
  hPrivateKey: HCRYPTKEY;
  hHash: HCRYPTHASH;
  Size: Cardinal;
  SignatureStream: TMemoryStream;
  hKeyImp:  HCRYPTKEY;
  hHashImp: HCRYPTHASH;
  password: String;
begin
  if not FileExists(PrivateKey) then
  begin
    ShowMessage(T(eCantFindPrivateKey));
    exit;
  end;
  if not FileExists(Target) then
  begin
    ShowMessage(T(eCantFindTarget));
    exit;
  end;
  if FileExists(Signature) and not DeleteFile(Signature) then
  begin
    ShowMessage(T(eCantFindSignature));
    exit;
  end;

  try
    // ������� ���������, ���� �� �����������
    CryptAcquireContext(@hProv, CONTAINER, nil, PROV_RSA_FULL,
                        CRYPT_DELETEKEYSET);

    // ������� ��������� ��� �������� ����
    if not CryptAcquireContext(@hProv, CONTAINER, nil, PROV_RSA_FULL, 0) then
      if not CryptAcquireContext(@hProv, CONTAINER, nil, PROV_RSA_FULL,
                                 CRYPT_NEWKEYSET) then
        raise Exception.Create(SysErrorMessage(GetLastError));

    Password := InputBox(T(mCreateKeyPair),
                         T(mInputPasswordForPrivateKey),
                         '');
    if Password = '' then
      raise Exception.Create(T(eEmptyPasswordForPrivateKey));

    // ������� ��� �� ������ ��� ������� ���������� �����
    CreateHashByString(password, hProv, hHashImp);
    try
      // ������� ��� �� ���� ��� ������� ���������� �����
      if not CryptDeriveKey(hProv, PRIVATEKEY_ALGID_KEY, hHashImp, 0, @hKeyImp) then
        raise Exception.Create(SysErrorMessage(GetLastError));

      try
        // ������������� ��������� ���� ��� �������� ������� ����
        ImportKey(PrivateKey, hProv, hPrivateKey, hKeyImp);
      finally
        // ������� ���� ��� ������� ���������� �����
        if not CryptDestroyKey(hKeyImp) then
          raise Exception.Create(SysErrorMessage(GetLastError));
      end;
    finally
      // ������� ���
      if not CryptDestroyHash(hHashImp) then
        raise Exception.Create(SysErrorMessage(GetLastError));
    end;

    try
      // ������� ��� �� �����
      CreateHash(Target, hProv, hHash);
      try
        // ������ ������ �������
        if not CryptSignHash(hHash, AT_SIGNATURE, nil, 0, nil, @Size) then
          raise Exception.Create(SysErrorMessage(GetLastError));

        SignatureStream := TMemoryStream.Create;
        try
          SignatureStream.SetSize(Size);
          // ������� �������
          if not CryptSignHash(hHash, AT_SIGNATURE, nil, 0,
                               SignatureStream.Memory, @Size) then
            raise Exception.Create(SysErrorMessage(GetLastError));

          // ��������� ������� � ����
          SignatureStream.SaveToFile(Signature);
        finally
          SignatureStream.Free;
        end;
      finally
        // ������� ���
        if not CryptDestroyHash(hHash) then
          raise Exception.Create(SysErrorMessage(GetLastError));
      end;
    finally
      // ������� �������� ����
      if not CryptDestroyKey(hPrivateKey) then
        raise Exception.Create(SysErrorMessage(GetLastError));
    end;
  finally
    // ���������� ���������
    if not CryptReleaseContext(hProv, 0) then
      raise Exception.Create(SysErrorMessage(GetLastError));
    // ������� ����� �� ����������
    if not CryptAcquireContext(@hProv, CONTAINER, nil, PROV_RSA_FULL,
                               CRYPT_DELETEKEYSET) then
      raise Exception.Create(SysErrorMessage(GetLastError));
  end;
end;

function Verify(PublicKey: String; Target: String; Signature: String): Boolean;
var
  hProv: HCRYPTPROV;
  hPublicKey: HCRYPTKEY;
  hHash: HCRYPTHASH;
  SignatureStream: TMemoryStream;
begin
  Result := False;
  if not FileExists(PublicKey) then
  begin
    ShowMessage(T(eCantFindPrivateKey));
    exit;
  end;
  if not FileExists(Target) then
  begin
    ShowMessage(T(eCantFindTarget));
    exit;
  end;
  if not FileExists(Signature) then
  begin
    ShowMessage(T(eCantFindSignature));
    exit;
  end;

  try
    // ������� ���������, ���� �� �����������
    CryptAcquireContext(@hProv, CONTAINER, nil, PROV_RSA_FULL,
                        CRYPT_DELETEKEYSET);

    // ������� ��������� ��� �������� ����
    if not CryptAcquireContext(@hProv, CONTAINER, nil, PROV_RSA_FULL, 0) then
      if not CryptAcquireContext(@hProv, CONTAINER, nil, PROV_RSA_FULL,
                                 CRYPT_NEWKEYSET) then
        raise Exception.Create(SysErrorMessage(GetLastError));

    // ������� ��� �� �����
    CreateHash(Target, hProv, hHash);
    try
      // ������������� ��������� ���� ��� �������� ������� ����
      ImportKey(PublicKey, hProv, hPublicKey, 0);
      try
        SignatureStream := TMemoryStream.Create;
        try
          // ��������� �������
          SignatureStream.LoadFromFile(Signature);
          SignatureStream.Position := 0;
        
          // ��������� ������� ���� �� ���������� �����
          if not CryptVerifySignature(hHash,
                                      SignatureStream.Memory,
                                      SignatureStream.Size,
                                      hPublicKey, nil, 0) then
            raise Exception.Create(SysErrorMessage(GetLastError));

          ShowMessage(T(mSinatureCorrect));
          Result := True;
        finally
          FreeAndNil(SignatureStream);
        end;
      finally
        // ������� �������� ����
        if not CryptDestroyKey(hPublicKey) then
          raise Exception.Create(SysErrorMessage(GetLastError));
      end;
    finally
      // ������� ���
      if not CryptDestroyHash(hHash) then
        raise Exception.Create(SysErrorMessage(GetLastError));
    end;
  finally
    // ���������� ���������
    if not CryptReleaseContext(hProv, 0) then
      raise Exception.Create(SysErrorMessage(GetLastError));
    // ������� ����� �� ����������
    if not CryptAcquireContext(@hProv, CONTAINER, nil, PROV_RSA_FULL,
                               CRYPT_DELETEKEYSET) then
      raise Exception.Create(SysErrorMessage(GetLastError));
  end;
end;

end.
