object Form1: TForm1
  Left = 0
  Top = 0
  Caption = #1055#1088#1080#1083#1086#1078#1077#1085#1080#1077' '#1076#1083#1103' '#1087#1086#1076#1087#1080#1089#1080' '#1080' '#1075#1077#1085#1077#1088#1072#1094#1080#1080' '#1082#1083#1102#1095#1077#1074#1086#1081' '#1087#1072#1088#1099
  ClientHeight = 148
  ClientWidth = 577
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 11
    Width = 87
    Height = 13
    Caption = #1055#1091#1073#1083#1080#1095#1085#1099#1081' '#1082#1083#1102#1095
  end
  object Label2: TLabel
    Left = 8
    Top = 38
    Width = 154
    Height = 13
    Caption = #1055#1091#1073#1083#1080#1095#1085#1099#1081' '#1082#1083#1102#1095' '#1076#1083#1103' '#1087#1088#1086#1077#1082#1090#1072
  end
  object Label3: TLabel
    Left = 8
    Top = 65
    Width = 87
    Height = 13
    Caption = #1055#1088#1080#1074#1072#1090#1085#1099#1081' '#1082#1083#1102#1095
  end
  object Label4: TLabel
    Left = 8
    Top = 92
    Width = 93
    Height = 13
    Caption = #1060#1072#1081#1083' '#1076#1083#1103' '#1087#1086#1076#1087#1080#1089#1080
  end
  object Label5: TLabel
    Left = 8
    Top = 119
    Width = 71
    Height = 13
    Caption = #1060#1072#1081#1083' '#1087#1086#1076#1087#1080#1089#1080
  end
  object Edit1: TEdit
    Left = 168
    Top = 8
    Width = 280
    Height = 21
    TabOrder = 0
  end
  object Button1: TButton
    Left = 454
    Top = 8
    Width = 34
    Height = 21
    Caption = '...'
    TabOrder = 1
    OnClick = Button1Click
  end
  object ButtonCreate: TButton
    Left = 494
    Top = 8
    Width = 75
    Height = 75
    Caption = #1057#1086#1079#1076#1072#1090#1100
    TabOrder = 2
    OnClick = ButtonCreateClick
  end
  object Edit2: TEdit
    Left = 168
    Top = 35
    Width = 280
    Height = 21
    TabOrder = 3
  end
  object Button2: TButton
    Left = 454
    Top = 35
    Width = 34
    Height = 21
    Caption = '...'
    TabOrder = 4
    OnClick = Button2Click
  end
  object Edit3: TEdit
    Left = 168
    Top = 62
    Width = 280
    Height = 21
    TabOrder = 5
  end
  object Edit4: TEdit
    Left = 168
    Top = 89
    Width = 280
    Height = 21
    TabOrder = 6
  end
  object Button3: TButton
    Left = 454
    Top = 62
    Width = 34
    Height = 21
    Caption = '...'
    TabOrder = 7
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 454
    Top = 89
    Width = 34
    Height = 21
    Caption = '...'
    TabOrder = 8
    OnClick = Button4Click
  end
  object ButtonSign: TButton
    Left = 494
    Top = 89
    Width = 75
    Height = 21
    Caption = #1055#1086#1076#1087#1080#1089#1072#1090#1100
    TabOrder = 9
    OnClick = ButtonSignClick
  end
  object ButtonVerify: TButton
    Left = 494
    Top = 116
    Width = 75
    Height = 21
    Caption = #1055#1088#1086#1074#1077#1088#1080#1090#1100
    TabOrder = 10
    OnClick = ButtonVerifyClick
  end
  object Edit5: TEdit
    Left = 168
    Top = 116
    Width = 280
    Height = 21
    TabOrder = 11
  end
  object Button5: TButton
    Left = 454
    Top = 116
    Width = 34
    Height = 21
    Caption = '...'
    TabOrder = 12
    OnClick = Button5Click
  end
  object OpenDialog1: TOpenDialog
    Left = 208
    Top = 16
  end
  object SaveDialog1: TSaveDialog
    Left = 208
    Top = 64
  end
end
