{*******************************************************************
@file    Form.pas
@author  Sadakov Oleg <olegsadakov.work@gmail.com>
@license LGPLv3, 29 June 2007

@section LICENSE

Application for the creation of the key pair,
signature creation and verification.

Copyright (C) 2013 Sadakov Oleg <olegsadakov.work@gmail.com>

GNU LESSER GENERAL PUBLIC LICENSE

Version 3, 29 June 2007

Copyright � 2007 Free Software Foundation, Inc. <http://fsf.org/>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************}

unit Form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IniFiles;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Label1: TLabel;
    Button1: TButton;
    ButtonCreate: TButton;
    Edit2: TEdit;
    Label2: TLabel;
    Button2: TButton;
    Label3: TLabel;
    Edit3: TEdit;
    Label4: TLabel;
    Edit4: TEdit;
    Button3: TButton;
    Button4: TButton;
    ButtonSign: TButton;
    ButtonVerify: TButton;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Label5: TLabel;
    Edit5: TEdit;
    Button5: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure ButtonCreateClick(Sender: TObject);
    procedure ButtonSignClick(Sender: TObject);
    procedure ButtonVerifyClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  CryptoUnit, Language;

procedure TForm1.Button1Click(Sender: TObject);
begin
  SaveDialog1.Title := T(tSelectPublicKey);
  SaveDialog1.InitialDir := GetCurrentDir;
  SaveDialog1.Filter := T(mKeyExt)+'|*.key|'+
                        T(mAllExt)+'|*';
  SaveDialog1.DefaultExt := 'key';
  SaveDialog1.FilterIndex := 1;
  SaveDialog1.FileName := '';
  if SaveDialog1.Execute then
    Edit1.Text := SaveDialog1.FileName;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  SaveDialog1.Title := T(tSelectPublicKeyInc);
  SaveDialog1.InitialDir := GetCurrentDir;
  SaveDialog1.Filter := T(mKeyIncExt)+'|*.inc|'+
                        T(mAllExt)+'|*';
  SaveDialog1.DefaultExt := 'inc';
  SaveDialog1.FilterIndex := 1;
  SaveDialog1.FileName := '';
  if SaveDialog1.Execute then
    Edit2.Text := SaveDialog1.FileName;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  SaveDialog1.Title := T(tSelectPrivateKey);
  SaveDialog1.InitialDir := GetCurrentDir;
  SaveDialog1.Filter := T(mKeyExt)+'|*.key|'+
                        T(mAllExt)+'|*';
  SaveDialog1.DefaultExt := 'key';
  SaveDialog1.FilterIndex := 1;
  SaveDialog1.FileName := '';
  if SaveDialog1.Execute then
    Edit3.Text := SaveDialog1.FileName;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  OpenDialog1.Title := T(tSelectTarget);
  OpenDialog1.InitialDir := GetCurrentDir;
  OpenDialog1.Filter := T(mAllExt)+'|*';
  OpenDialog1.FileName := '';
  if OpenDialog1.Execute then
    Edit4.Text := OpenDialog1.FileName;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  SaveDialog1.Title := T(tSelectSignature);
  SaveDialog1.InitialDir := GetCurrentDir;
  SaveDialog1.Filter := T(mSignatureExt)+'|*.sign|'+
                        T(mAllExt)+'|*';
  SaveDialog1.DefaultExt := 'sign';
  SaveDialog1.FilterIndex := 1;
  SaveDialog1.FileName := '';
  if SaveDialog1.Execute then
    Edit5.Text := SaveDialog1.FileName;
end;

procedure TForm1.ButtonCreateClick(Sender: TObject);
begin
  try
    CreatePair(Edit1.Text, Edit2.Text, Edit3.Text);
  except
    on e: Exception do
      ShowMessage(e.Message);
  end;
end;

procedure TForm1.ButtonSignClick(Sender: TObject);
begin
  try
    Sign(Edit3.Text, Edit4.Text, Edit5.Text);
  except
    on e: Exception do
      ShowMessage(e.Message);
  end;
end;

procedure TForm1.ButtonVerifyClick(Sender: TObject);
begin
  try
    Verify(Edit1.Text, Edit4.Text, Edit5.Text);
  except
    on e: Exception do
      ShowMessage(e.Message);
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  ini: TIniFile;
begin
  ini := nil;
  try
    ini := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
    Edit1.Text := ini.ReadString('KeyPair',   'Public',   '');
    Edit2.Text := ini.ReadString('KeyExport', 'Public',   '');
    Edit3.Text := ini.ReadString('KeyPair',   'Private',  '');
    Edit4.Text := ini.ReadString('Target',    'Filename', '');
    Edit5.Text := ini.ReadString('Signature', 'Filename', '');
  finally
    FreeAndNil(ini);
  end;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
var
  ini: TIniFile;
begin
  ini := nil;
  try
    ini := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
    ini.WriteString('KeyPair',   'Public',   Edit1.Text);
    ini.WriteString('KeyExport', 'Public',   Edit2.Text);
    ini.WriteString('KeyPair',   'Private',  Edit3.Text);
    ini.WriteString('Target',    'Filename', Edit4.Text);
    ini.WriteString('Signature', 'Filename', Edit5.Text);
  finally
    FreeAndNil(ini);
  end;
end;

end.
