{*******************************************************************
@file    Language.pas
@author  Sadakov Oleg <olegsadakov.work@gmail.com>
@license LGPLv3, 29 June 2007

@section LICENSE

Application for the creation of the key pair,
signature creation and verification.

Copyright (C) 2013 Sadakov Oleg <olegsadakov.work@gmail.com>

GNU LESSER GENERAL PUBLIC LICENSE

Version 3, 29 June 2007

Copyright � 2007 Free Software Foundation, Inc. <http://fsf.org/>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************}

unit Language;

interface

type
  ELanguageIndex =
  (
    eCantRemovePublicKey,
    eCantRemovePublicKeyInc,
    eCantRemovePrivateKey,
    mCreateKeyPair,
    mInputPasswordForPrivateKey,
    eEmptyPasswordForPrivateKey,
    eCantFindPublicKey,
    eCantFindPrivateKey,
    eCantFindTarget,
    eCantFindSignature,
    mSinatureCorrect,

    tSelectPublicKey,
    tSelectPublicKeyInc,
    tSelectPrivateKey,
    tSelectTarget,
    tSelectSignature,
    mKeyExt,
    mKeyIncExt,
    mSignatureExt,
    mAllExt,

    tTitleApplication,
    tUseApplication,
    tIncorrectParameters
  );

function T(Index: ELanguageIndex): String;

implementation

function T(Index: ELanguageIndex): String;
begin
  case Index of
    eCantRemovePublicKey:
      Result := '�� ������� ������� ������ ���� ���������� �����';
    eCantRemovePublicKeyInc:
      Result := '�� ������� ������� ������ ���� ���������� ����� ��� �������';
    eCantRemovePrivateKey:
      Result := '�� ������� ������� ������ ���� ���������� �����';
    mCreateKeyPair:
      Result := '�������� �������� ����';
    mInputPasswordForPrivateKey:
      Result := '������� ������ ��� ���������� �����';
    eEmptyPasswordForPrivateKey:
      Result := '������ ������ ��� ���������� �����';
    eCantFindPublicKey:
      Result := '�� ������ ���� ���������� �����';
    eCantFindPrivateKey:
      Result := '�� ������ ���� ���������� �����';
    eCantFindTarget:
      Result := '�� ������ ���� ��� �������';
    eCantFindSignature:
      Result := '�� ������� ������� ������ ���� �������';
    mSinatureCorrect:
      Result := '������� ������������';
    tSelectPublicKey:
      Result := '������� ���� ���������� �����';
    tSelectPublicKeyInc:
      Result := '������� ���� ���������� ����� ��� ����������� � �������';
    tSelectPrivateKey:
      Result := '������� ���� ���������� �����';
    tSelectTarget:
      Result := '������� ���� ��� �������';
    tSelectSignature:
      Result := '������� ���� ��� ���������� �������';
    mKeyExt:
      Result := '����� ������';
    mKeyIncExt:
      Result := '����� ������ ��� ����������� � �������';
    mSignatureExt:
      Result := '����� ��������';
    mAllExt:
      Result := '��� �����';
    tTitleApplication:
      Result := '���������� ��� ��������/�������� �������� � '+
                '�������� �������� ���';
    tUseApplication:
      Result := '���������:'+#13#10+
                '  "action",   '+#9+'"-a"'+#9+ ' - ��� ��������:'+
                                               ' sign - �������,'+
                                               ' verify - �������� �������.'+
                                               #13#10+
                '  "key",      '+#9+'"-k"'+#9+ ' - ���� �����.'+#13#10+
                '  "target",   '+#9+'"-t"'+#9+ ' - ���� � �������.'+#13#10+
                '  "signature",'+#9+'"-s"'+#9+ ' - ���� �������.';
    tIncorrectParameters:
      Result := '�������� ���������:';
  end;
end;

end.
